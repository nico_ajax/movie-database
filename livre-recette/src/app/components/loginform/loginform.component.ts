import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { User } from '../../models/user';
import { AuthService } from '../../services/auth.service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {

  model = new User();

  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  logIn(): void {
    if (!this.model.email) {
      alert('You must enter your email!');
    } else if (!this.model.password) {
      alert('You must enter your password!');
    } else {
      this.authService.login(this.model).subscribe(
        obj => {
          this.authService.setToken(obj.user.token);
          console.log(obj.user.token);
          console.log(obj.user.email + ' logged in');
          this.router.navigate(['display']);
        });
    }
  }
}
