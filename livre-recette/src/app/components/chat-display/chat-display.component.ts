import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models/message';

@Component({
  selector: 'app-chat-display',
  templateUrl: './chat-display.component.html',
  styleUrls: ['./chat-display.component.css']
})
export class ChatDisplayComponent implements OnInit {

  @Input() list: Array<Message>;


  constructor() { }

  ngOnInit() {
  }

  delete(i: number): void {
    this.list.splice(i, 1);
  }

}
