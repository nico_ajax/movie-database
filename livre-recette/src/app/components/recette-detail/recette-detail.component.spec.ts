import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';

import { RecetteDetailComponent } from './recette-detail.component';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub } from '../../testing/activated-route-stub';

let listeRecetteServiceStub: Partial<ListeRecetteService>;

listeRecetteServiceStub = {
  recipe: {
    _id: 'test', name: 'test', difficulty: '★', country: 'test', duration: 'tes',
    type: 'Quotidien', ingredients: [{ name: 'test', quantity: 'tes' }], explication: ''
  }
};

const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl', 'events']);

@Component({ selector: 'app-chat', template: '' })
class ChatStubComponent {
}

describe('RecetteDetailComponent', () => {
  let component: RecetteDetailComponent;
  let fixture: ComponentFixture<RecetteDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecetteDetailComponent, ChatStubComponent],
      providers: [
        { provide: ListeRecetteService, useValue: listeRecetteServiceStub },
        { provide: Router, useValue: routerSpy },
        { provide: ActivatedRoute, useValue: ActivatedRouteStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecetteDetailComponent);
    component = fixture.componentInstance;
    /// fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
