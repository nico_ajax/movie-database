import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';


import { Recipe } from '../../models/recipe';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-recette-detail',
  templateUrl: './recette-detail.component.html',
  styleUrls: ['./recette-detail.component.css']
})
export class RecetteDetailComponent implements OnInit {

  @Input() recette: Recipe;
  clicked = true;

  constructor(private route: ActivatedRoute,
    private listeRecetteService: ListeRecetteService,
    private router: Router) { }

  ngOnInit(): void {
    this.getRecette();
  }

  getRecette() {
    const id = this.route.snapshot.paramMap.get('id');
    this.listeRecetteService.getRecette(id)
      .subscribe(recette => this.recette = recette);
  }

  goBack(): void {
    this.router.navigate(['/display']);
  }

  delete(id: string): void {
    if (confirm('Êtes vous sur de vouloir supprimer cet élément ?')) {
      this.listeRecetteService.deleteRecette(id).subscribe(
        _ => this.router.navigate(['/display'])
      );
    }
  }

  sendRecipe(): void {
    this.listeRecetteService.receiveRecette(this.recette);
    console.log(this.listeRecetteService.recipe.nom);
  }

  deployChat(): void {
    if (!this.clicked) {
      this.clicked = true;
    } else {
      this.clicked = false;
    }
  }

}
