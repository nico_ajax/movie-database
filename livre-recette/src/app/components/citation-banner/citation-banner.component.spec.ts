import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitationBannerComponent } from './citation-banner.component';

describe('CitationBannerComponent', () => {
  let component: CitationBannerComponent;
  let fixture: ComponentFixture<CitationBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CitationBannerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitationBannerComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
