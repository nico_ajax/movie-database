import { Component, OnInit, OnDestroy } from '@angular/core';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';

import { Recipe } from '../../models/recipe';
import { Search } from '../../models/search';

import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service/auth.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit, OnDestroy {
  searching = false;
  search = new Search();
  list = new Array<Recipe>();
  routeEvents: Subscription;

  notConnected = true;

  constructor(private listeRecetteService: ListeRecetteService,
    private router: Router,
    private authService: AuthService) {
    this.routeEvents = router.events.subscribe(event => {
      if (event instanceof NavigationEnd) { this.chargingPage(); }
    });
  }

  ngOnInit() {
    console.log(localStorage);
  }

  ngOnDestroy() {
    this.routeEvents.unsubscribe();
  }

  chargingPage() {
    this.getSearchState();
    if (this.searching) {
      this.getSearch();
      this.getSearchList(this.search.name, this.search.type);
    } else {
      this.getList();
    }
  }

  getList(): void {
    this.listeRecetteService.getRecettes()
      .subscribe(list => this.list = list);
  }

  getSearchList(name: string, category: string) {
    this.listeRecetteService.searchRecette()
      .subscribe(list => this.list = list);
    this.searching = false;
    this.listeRecetteService.searching = false;
  }

  getSearchState(): void {
    this.listeRecetteService.getSearchState()
      .subscribe(searching => this.searching = searching);
  }

  getSearch(): void {
    this.listeRecetteService.sendSearch()
      .subscribe(search => this.search = search);
  }

  isOnline(): boolean {
    this.notConnected = this.authService.isTokenExpired();
    return (!this.notConnected);
  }

}
