import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayComponent } from './display.component';

import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Router, RouterEvent } from '@angular/router';
import { RouterLinkDirectiveStub } from '../../testing/router-link-directive-stub';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../services/auth.service/auth.service';

let listeRecetteServiceStub: Partial<ListeRecetteService>;

listeRecetteServiceStub = {
  recipe: {
    _id: 'tes', name: 'test', difficulty: '★', country: 'test', duration: 'tes',
    type: 'Quotidien', ingredients: [{ name: 'test', quantity: 'tes' }], explication: ''
  }
};

let AuthServiceStub: Partial<AuthService>;

AuthServiceStub = {};


let mockRouter: jasmine.SpyObj<Router>;

const routerEvents = new Array<RouterEvent>();

mockRouter = jasmine.createSpyObj('mockRouter', ['navigate']);

mockRouter = {
  ...mockRouter,
  events: Observable.of(...routerEvents),
} as jasmine.SpyObj<Router>;



describe('DisplayComponent', () => {
  let component: DisplayComponent;
  let fixture: ComponentFixture<DisplayComponent>;
  let linkDes: DebugElement[];
  let routerLinks: RouterLinkDirectiveStub[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DisplayComponent, RouterLinkDirectiveStub],
      providers: [{ provide: ListeRecetteService, useValue: listeRecetteServiceStub },
      { provide: Router, useValue: mockRouter },
      { provide: AuthService, useValue: AuthServiceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayComponent);
    component = fixture.componentInstance;
    /// fixture.detectChanges();
    linkDes = fixture.debugElement
      .queryAll(By.directive(RouterLinkDirectiveStub));

    // get attached link directive instances
    // using each DebugElement's injector
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
