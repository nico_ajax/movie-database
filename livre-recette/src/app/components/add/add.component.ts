// Importing Angular Elements
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Importing Typrescipt objects
import { Acteur } from '../../models/ingredient';
import { Recipe } from '../../models/recipe';

// Importing services
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';

// This component defines the interface to add recipes to the database
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  // A model to store the recipe to add
  model: Recipe;

  // The different options for the difficulties and types fields
  difficulties = ['★', '★★', '★★★', '★★★★', '★★★★★'];
  types = ['SF', 'Peplum', 'Historique', 'Thriller', 'Drame psychologique', 'Comédie', 'Film de guerre', 'Séries'];

  constructor(private listeRecetteService: ListeRecetteService,
    private router: Router) { }

  // Initializing the model and its ingredients
  ngOnInit() {
    this.model = new Recipe();
    this.model.acteurs = [];
  }

  // A function to add new ingredients to the recipe
  addIngredient(newIngredientName: string) {
    if (newIngredientName) {
      this.model.acteurs.push({ name: newIngredientName });
    }
  }

  // A function to delete ingredient from the recipe
  deleteIngredient(name: string): void {
    this.model.acteurs.splice(this.model.acteurs.indexOf(this.model.acteurs.find(model => model.name === name), 1));
  }

  /** A function triggered when submtting the form calling the addRecette method
   * of the service to post the new recipe and redirect the user to the display screen*/
  onSubmit() {
    this.listeRecetteService.addRecette(this.model).subscribe(
      _ => this.router.navigate(['/display'])
    );
  }

}
