import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import { LoginComponent } from './login.component';
import { Component } from '@angular/core';

@Component({ selector: 'app-loginform', template: '' })
class LoginFormStubComponent {
}

@Component({ selector: 'app-signupform', template: '' })
class SignupFormStubComponent {
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, SignupFormStubComponent, LoginFormStubComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
