import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { CitationComponent } from '../citation/citation.component';
import { CitationService } from '../../services/citation.service/citation.service';

let listeRecetteServiceStub: Partial<ListeRecetteService>;

listeRecetteServiceStub = {
  search: { name: 'test', type: 'test' }
};

let citationServiceStub: Partial<CitationService>;

citationServiceStub = {};

@Component({ selector: 'app-citation-banner', template: '' })
class CitationBannerStubComponent {
}

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent, CitationBannerStubComponent],
      providers: [
        { provide: ListeRecetteService, useValue: listeRecetteServiceStub },
        { provide: CitationService, useValue: citationServiceStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
