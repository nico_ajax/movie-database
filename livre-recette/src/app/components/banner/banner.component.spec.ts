import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerComponent } from './banner.component';
import { AuthService } from '../../services/auth.service/auth.service';
import { Router } from '@angular/router';

let AuthServiceStub: Partial<AuthService>;

AuthServiceStub = {};

const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

describe('BannerComponent', () => {
  let component: BannerComponent;
  let fixture: ComponentFixture<BannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BannerComponent],
      providers: [{ provide: AuthService, useValue: AuthServiceStub }, { provide: Router, useValue: routerSpy }],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerComponent);
    component = fixture.componentInstance;
    /// fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
