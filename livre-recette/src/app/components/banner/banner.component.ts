import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  notConnected = true;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  logOut(): void {
    this.authService.logout();
    this.router.navigate(['/display']);
  }

  isOnline(): boolean {
    this.notConnected = this.authService.isTokenExpired();
    return (!this.notConnected);
  }

}
