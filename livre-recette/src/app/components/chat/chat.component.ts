import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models/message';
import { ChatService } from '../../services/chat.service/chat.service';
import { AuthService } from '../../services/auth.service/auth.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  @Input() name: String;

  model = new Message();
  messages = new Array<Message>();

  constructor(private chatService: ChatService, private authService: AuthService) { }

  ngOnInit() {
    this.model.author = this.authService.getUser(this.authService.getToken()).split('@')[0];
    this.model.room = this.name;
    this.chatService.joinroom(this.model);
    this.getMessages();
  }

  postMessage(message: String): void {
    this.model.message = message;
    this.chatService.sendMessage(this.model);
    console.log('message sent');
  }

  getMessages(): void {
    this.chatService.getMessages()
      .subscribe(list => this.messages.push(list));
    console.log('messages updated');
  }
}
