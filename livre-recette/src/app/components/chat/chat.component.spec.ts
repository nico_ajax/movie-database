import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatComponent } from './chat.component';
import { ChatService } from '../../services/chat.service/chat.service';

import { FormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth.service/auth.service';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';

let chatServiceStub: Partial<ChatService>;

chatServiceStub = {
  messages: [{ author: 'Anonymous', message: 'hello!', room: '1' }, { author: 'Anonymous', message: 'Hey bitch!', room: '1' }]
};

let authServiceStub: Partial<AuthService>;

authServiceStub = {};

@Component({ selector: 'app-chat-display', template: '' })
class ChatDisplayStubComponent {
}

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ChatComponent, ChatDisplayStubComponent],
      providers: [{ provide: ChatService, useValue: chatServiceStub }, { provide: AuthService, useValue: authServiceStub }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    /// fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
