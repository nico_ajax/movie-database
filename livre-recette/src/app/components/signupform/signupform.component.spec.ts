import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { SignupformComponent } from './signupform.component';
import { AuthService } from '../../services/auth.service/auth.service';
import { Router } from '@angular/router';

let AuthServiceStub: Partial<AuthService>;

AuthServiceStub = {};

const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

describe('SignupformComponent', () => {
  let component: SignupformComponent;
  let fixture: ComponentFixture<SignupformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignupformComponent],
      imports: [FormsModule],
      providers: [{ provide: AuthService, useValue: AuthServiceStub },
      { provide: Router, usevalue: routerSpy }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
