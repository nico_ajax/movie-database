import { Component, OnInit } from '@angular/core';
import { ListeRecetteService } from '../../services/liste-recette.service/liste-recette.service';
import { Router } from '@angular/router';
import { Recipe } from '../../models/recipe';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.css']
})
export class ModifyComponent implements OnInit {

  model = new Recipe();
  difficulties = ['★', '★★', '★★★', '★★★★', '★★★★★'];
  types = ['SF', 'Peplum', 'Historique', 'Thriller', 'Drame psychologique', 'Comédie', 'Film de guerre', 'Séries'];

  constructor(private listeRecetteService: ListeRecetteService,
    private router: Router) { }

  ngOnInit() {
    this.getRecipe();
  }

  addIngredient(newIngredientName: string) {
    if (newIngredientName) {
      this.model.acteurs.push({ name: newIngredientName });
    }
  }

  deleteIngredient(name: string): void {
    this.model.acteurs.splice(this.model.acteurs.indexOf(this.model.acteurs.find(model => model.name === name), 1));
  }

  getRecipe(): void {
    this.listeRecetteService.sendRecette()
      .subscribe(recette => this.model = recette);
    console.log(this.model.acteurs);
  }

  onSubmit() {
    this.listeRecetteService.modifyRecette(this.model).subscribe(
      _ => this.router.navigate(['/display/' + this.model._id])
    );
  }

}
