import { TestBed, inject } from '@angular/core/testing';

import { ListeRecetteService } from './liste-recette.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth.service/auth.service';

let httpClientSpy: { get: jasmine.Spy };

let authStubService: Partial<AuthService>;
authStubService = {};

describe('ListeRecetteService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'delete', 'post']);
    TestBed.configureTestingModule({
      providers: [ListeRecetteService,
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: AuthService, useValue: authStubService }]
    });
  });

  it('should be created', inject([ListeRecetteService], (service: ListeRecetteService) => {
    expect(service).toBeTruthy();
  }));
});
