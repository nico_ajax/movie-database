import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Recipe } from '../../models/recipe';
import { Search } from '../../models/search';
import { AuthService } from '../auth.service/auth.service';


@Injectable()
export class ListeRecetteService {

  search = new Search();
  searching = false;

  recipe = new Recipe();

  token: String;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };


  constructor(private http: HttpClient, private authService: AuthService) { }

  getRecettes(): Observable<Recipe[]> {
    this.token = this.authService.getToken();
    const tok = this.token;
    if (tok) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Token ' + tok,
        })
      };
    } else {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return this.http.get<Recipe[]>('http://localhost:3000/recipes/getrecipes', this.httpOptions)
      .pipe(
        tap(recettes => console.log(`fetched recette`)),
        catchError(this.handleError('getRecettes', []))
      );
  }

  getRecette(id: string): Observable<Recipe> {
    this.token = this.authService.getToken();
    const tok = this.token;
    if (tok) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Token ' + tok,
        })
      };
    } else {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return this.http.get<Recipe>('http://localhost:3000/recipes/getrecipe/' + id, this.httpOptions)
      .pipe(
        tap(recette => console.log(`fetched recette`)),
        catchError(this.handleError<Recipe>(`getHero id=${id}`))
      );
  }

  searchRecette(): Observable<Recipe[]> {
    this.token = this.authService.getToken();
    const tok = this.token;
    if (tok) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Token ' + tok,
        })
      };
    } else {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return this.http.get<Recipe[]>('http://localhost:3000/recipes/getrecipes/' + this.search.type + '/' + this.search.name)
      .pipe(
        tap(_ => console.log('search sent')),
        catchError(this.handleError<Recipe[]>('search Recette'))
      );
  }

  getSearchState(): Observable<boolean> {
    return Observable.of(this.searching);
  }

  getSearchParam(params: Search): void {
    this.search.name = params.name;
    this.search.type = params.type;
    this.searching = true;
  }

  sendSearch(): Observable<Search> {
    return Observable.of(this.search);
  }

  receiveRecette(recette: Recipe): void {
    this.recipe = recette;
  }

  sendRecette(): Observable<Recipe> {
    return Observable.of(this.recipe);
  }

  modifyRecette(recette: Recipe): Observable<Recipe> {
    this.token = this.authService.getToken();
    const tok = this.token;
    if (tok) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Token ' + tok,
        })
      };
    } else {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return this.http.put('http://localhost:3000/recipes/modify/' + recette._id, recette, this.httpOptions).pipe(
      tap(_ => console.log(`updated recette`)),
      catchError(this.handleError<any>('modifyRecette'))
    );
  }


  deleteRecette(id: string): Observable<Recipe> {
    this.token = this.authService.getToken();
    const tok = this.token;
    if (tok) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Token ' + tok,
        })
      };
    } else {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return this.http.delete<Recipe>('http://localhost:3000/recipes/delete/' + id, this.httpOptions)
      .pipe(
        tap(_ => console.log(`deleted`)),
        catchError(this.handleError<Recipe>('deleteRecette'))
      );
  }



  addRecette(recette: Recipe): Observable<Recipe> {
    return this.http.post<Recipe>('http://localhost:3000/recipes/addrecipe', recette, this.httpOptions)
      .pipe(
        tap(_ => console.log('added recette')),
        catchError(this.handleError<Recipe>('addRecette'))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return Observable.of(result as T);
    };
  }

}
