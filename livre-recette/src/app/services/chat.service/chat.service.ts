import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Message } from '../../models/message';
import { of } from 'rxjs/observable/of';
import * as io from 'socket.io-client';

@Injectable()
export class ChatService {

  private socket;
  private url = 'http://localhost:3000';

  messages = new Array<Message>();

  constructor() {
    this.socket = io(this.url);
  }


  joinroom(message: Message) {
    this.socket.emit('subscribe', message);
  }

  sendMessage(message: Message) {
    this.socket.emit('sendMessage', message);
  }


  getMessages(): Observable<Message> {
    const observable = new Observable<Message>((observer) => {
      this.socket.on('new-message', (message) => {
        observer.next(message);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
}
