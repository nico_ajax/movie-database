import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private authService: AuthService) { }

    canActivate() {
        if (!this.authService.isTokenExpired()) {
            return true;
        }


        // tslint:disable-next-line:max-line-length && disable-next-line:quotemark
        if (confirm("Vous devez être connecter pour accéder à ces fonctionnalités, souhaitez-vous poursuivre vers l'écran de connection?")) {
            this.router.navigate(['/login']);
        }
        return false;
    }

}
