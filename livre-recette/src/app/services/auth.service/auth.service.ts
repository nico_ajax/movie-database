import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { User } from '../../models/user';

import * as jwt_decode from 'jwt-decode';
import { Observable } from 'rxjs/Observable';
import { tap, catchError } from 'rxjs/operators';
import { SubscribedUser } from '../../models/subscribedUser';

export const TOKEN_NAME = 'jwt_token';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) { }


  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) {
      return null;
    }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) {
      token = this.getToken();
    }

    if (!token) {
      return true;
    }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) {
      return false;
    }

    return !(date.valueOf() > new Date().valueOf());
  }

  getUser(token?: string): string {
    if (!token) {
      return 'Anonymous';
    } else {
      const decoded = jwt_decode(token);
      console.log(decoded);
      return (decoded.email);
    }
  }

  login(user: User): Observable<{ user: SubscribedUser }> {
    return this.http.post<{ user: SubscribedUser }>(`http://localhost:3000/auth/login`, { user }, httpOptions)
      .pipe(
        tap(_ => console.log('added recette')),
        catchError(this.handleError<{ user: SubscribedUser }>('login'))
      );
  }

  signup(user: User): Observable<{ user: SubscribedUser }> {
    return this.http.post<{ user: SubscribedUser }>(`http://localhost:3000/auth/signup`, { user }, httpOptions)
      .pipe(
        tap(_ => console.log('added recette')),
        catchError(this.handleError<{ user: SubscribedUser }>('signup'))
      );
  }

  logout(): void {
    localStorage.clear();
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return Observable.of(result as T);
    };
  }

}
