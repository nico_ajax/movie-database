import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[citation-host]',
})
export class CitationDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
