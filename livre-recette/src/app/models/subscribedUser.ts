export class SubscribedUser {
    email: string;
    password: string;
    token: string;
}
