export class Message {
    author: String;
    message: String;
    room: String;
}
