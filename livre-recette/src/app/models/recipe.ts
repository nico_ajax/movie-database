import { Acteur } from './ingredient';

export class Recipe {
    _id: string;
    nom: string;
    realisateur: string;
    date: string;
    duree: string;
    type: string;
    acteurs: Acteur[];
}
