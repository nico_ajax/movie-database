import { Type } from '@angular/core';

export class CitationItem {
    constructor(public component: Type<any>, public citation: String) { }
}
