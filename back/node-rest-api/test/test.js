let http = require("http");
let app = require("../app");
let serveur = http.createServer(app);
let request = require("supertest");

let recipe = {
  name: "Pate pesto",
  difficulty: "★",
  country: "Italie",
  duration: 10,
  type: "plat",
  ingredients: [
    {
      name: "Pate",
      quantity: "100g"
    },
    {
      name: "beurre",
      quantity: "2g"
    }
  ],
  explication: ""
};

describe("Testing the add recipe route", function() {
  /// Having the app to listen before each test
  before(function(done) {
    serveur.listen(function(err) {
      if (err) {
        return done(err);
      }
      done();
    });
  });

  after(function(done) {
    serveur.close();
    process.exit();
    done();
  });

  it("should return the posted json object", function(done) {
    request(serveur)
      .post("/recipes/addrecipe")
      .set("Content-Type", "application/json")
      .send(recipe)
      .expect("Content-Type", /json/)
      .expect(200);
    done();
  });
});
