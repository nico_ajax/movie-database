var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Recipe = require("../model/Recipe.js");
const passport = require("passport");

let func = require("../controller/recipeController");

const auth = require("../middleware/auth_req");

/* GET ALL PRODUCTS */
router.get("/getrecipes", auth.optional, func.getRecipes);

/* GET SINGLE PRODUCT BY ID */
router.get("/getrecipe/:id", auth.optional, func.getRecipe);

/* GET PRODUCTS THROUGH A SEARCH OF A PARTICULAR FIELD */
router.get("/getrecipes/:category/:content", auth.optional, func.searchRecipes);

/* SAVE PRODUCT */
router.post("/addrecipe", auth.optional, func.addRecipe);

/* UPDATE PRODUCT */
router.put("/modify/:id", auth.optional, func.modifyRecipe);

/* DELETE PRODUCT */
router.delete("/delete/:id", auth.optional, func.deleteRecipe);

module.exports = router;
