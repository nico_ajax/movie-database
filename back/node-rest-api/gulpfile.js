var gulp = require("gulp");

var minifyCSS = require("gulp-minify-css");
var uglifyJS = require("gulp-uglify-es").default;
var concat = require("gulp-concat");
var filter = require("gulp-filter");

gulp.task("compress", function() {
  return gulp
    .src("./model/Recipe.js")
    .pipe(uglifyJS())
    .pipe(gulp.dest("./gulped-files/model"));
});

gulp.task("minify-css", function() {
  return gulp
    .src("./public/stylesheets/style.css")
    .pipe(minifyCSS())
    .pipe(gulp.dest("./gulped-files/css"));
});

gulp.task("concat", function() {
  return gulp
    .src("./model/*.js")
    .pipe(concat("concat-model.js"))
    .pipe(gulp.dest("./gulped-files/concatened-files"));
});

gulp.task("concat-all", function() {
  return gulp
    .src([
      "./*.js",
      "./test/*.js",
      "./routes/*.js",
      "./model/*.js",
      ".config/*.js",
      "./bin/*.js"
    ])
    .pipe(concat("all.js"))
    .pipe(gulp.dest("./gulped-files/concatened-files"));
});

gulp.task("final", ["concat-all"], function() {
  return gulp
    .src("./gulped-files/concatened-files/all.js")
    .pipe(uglifyJS())
    .pipe(gulp.dest("./gulped-files/final/"));
});
