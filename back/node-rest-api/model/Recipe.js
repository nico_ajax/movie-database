let mongoose = require("mongoose");

let RecipeSchema = new mongoose.Schema({
  nom: String,
  realisateur: String,
  date: String,
  duree: String,
  type: String,
  acteurs: Array,
  franchise: String,
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("recette", RecipeSchema);
