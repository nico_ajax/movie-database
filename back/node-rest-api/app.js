var express = require("express");
var session = require("express-session");

var mongoose = require("mongoose");
mongoose.Promise = global.Promise;

///Models & Routes
var recipes = require("./routes/recipe");
var auth = require("./routes/auth");
require("./model/User");
require("./config/passport");

mongoose
  .connect("mongodb://database/recipes")
  .then(() => console.log("connection succesful"))
  .catch(err => console.error(err));

var app = express();

var socket_io = require("socket.io");

var io = socket_io();
app.io = io;

io.on("connection", function(socket) {
  console.log("user connected");

  //_On envoie a l'utilisateur qu'il est connecté
  socket.on("subscribe", function(data) {
    console.log("join realized!");
    socket.join(data.room);
  });

  //_Quand un utilisateur écrit un message, on le met dans la DB, et on l'affiche
  socket.on("sendMessage", function(data) {
    console.log("message :" + data.message);
    //_EQUIVALENT au POST /new/message
    var newMessage = {
      author: data.author,
      message: data.message,
      room: data.room
    };

    io.in(data.room).emit("new-message", newMessage);
  });

  socket.on("unsubscribe", function(data) {
    socket.leave(data.room);
  });
});

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type, authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use(express.json());

app.use("/recipes", recipes);
app.use("/auth", auth);

module.exports = app;
