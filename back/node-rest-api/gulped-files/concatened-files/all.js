var express = require("express");

var mongoose = require("mongoose");
mongoose.Promise = global.Promise;

///Models & Routes
var recipes = require("./routes/recipe");
var auth = require("./routes/auth");
require("./model/User");
require("./config/passport");

mongoose
  .connect("mongodb://database/recipes")
  .then(() => console.log("connection succesful"))
  .catch(err => console.error(err));

var app = express();

var socket_io = require("socket.io");

var io = socket_io();
app.io = io;

io.on("connection", function(socket) {
  console.log("user connected");

  //_On envoie a l'utilisateur qu'il est connecté
  socket.on("subscribe", function(data) {
    console.log("join realized!");
    socket.join(data.room);
  });

  //_Quand un utilisateur écrit un message, on le met dans la DB, et on l'affiche
  socket.on("sendMessage", function(data) {
    console.log("message :" + data.message);
    //_EQUIVALENT au POST /new/message
    var newMessage = {
      author: data.author,
      message: data.message,
      room: data.room
    };

    io.in(data.room).emit("new-message", newMessage);
  });

  socket.on("unsubscribe", function(data) {
    socket.leave(data.room);
  });
});

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use(express.json());

app.use("/recipes", recipes);
app.use("/auth", auth);

module.exports = app;

var gulp = require("gulp");

var minifyCSS = require("gulp-minify-css");
var uglifyJS = require("gulp-uglify-es").default;
var concat = require("gulp-concat");
var filter = require("gulp-filter");

gulp.task("compress", function() {
  return gulp
    .src("./model/Recipe.js")
    .pipe(uglifyJS())
    .pipe(gulp.dest("./gulped-files/model"));
});

gulp.task("minify-css", function() {
  return gulp
    .src("./public/stylesheets/style.css")
    .pipe(minifyCSS())
    .pipe(gulp.dest("./gulped-files/css"));
});

gulp.task("concat", function() {
  return gulp
    .src("./model/*.js")
    .pipe(concat("concat-model.js"))
    .pipe(gulp.dest("./gulped-files/concatened-files"));
});

gulp.task("concat-all", function() {
  return gulp
    .src([
      "./*.js",
      "./test/*.js",
      "./routes/*.js",
      "./model/*.js",
      ".config/*.js",
      "./bin/*.js"
    ])
    .pipe(concat("all.js"))
    .pipe(gulp.dest("./gulped-files/concatened-files"));
});

gulp.task("final", ["concat-all"], function() {
  return gulp
    .src("./gulped-files/concatened-files/all.js")
    .pipe(uglifyJS())
    .pipe(gulp.dest("./gulped-files/final/"));
});

let http = require("http");
let app = require("../app");
let serveur = http.createServer(app);
let request = require("supertest");

let recipe = {
  name: "Pate pesto",
  difficulty: "★",
  country: "Italie",
  duration: 10,
  type: "plat",
  ingredients: [
    {
      name: "Pate",
      quantity: "100g"
    },
    {
      name: "beurre",
      quantity: "2g"
    }
  ],
  explication: ""
};

describe("Testing the add recipe route", function() {
  /// Having the app to listen before each test
  before(function(done) {
    serveur.listen(function(err) {
      if (err) {
        return done(err);
      }
      done();
    });
  });

  after(function(done) {
    serveur.close();
    process.exit();
    done();
  });

  it("should return the posted json object", function(done) {
    request(serveur)
      .post("/recipes/addrecipe")
      .set("Content-Type", "application/json")
      .send(recipe)
      .expect("Content-Type", /json/)
      .expect(200);
    done();
  });
});

const jwt = require("express-jwt");

var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Users = require("../model/User");

const passport = require("passport");

const getTokenFromHeaders = req => {
  const {
    headers: { authorization }
  } = req;

  if (authorization && authorization.split(" ")[0] === "Token") {
    return authorization.split(" ")[1];
  }
  return null;
};

const auth = {
  required: jwt({
    secret: "secret",
    userProperty: "payload",
    getToken: getTokenFromHeaders
  }),
  optional: jwt({
    secret: "secret",
    userProperty: "payload",
    getToken: getTokenFromHeaders,
    credentialsRequired: false
  })
};

//POST new user route (optional, everyone has access)
router.post("/", auth.optional, (req, res, next) => {
  const {
    body: { user }
  } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: "is required"
      }
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: "is required"
      }
    });
  }

  const finalUser = new Users(user);

  finalUser.setPassword(user.password);

  return finalUser
    .save()
    .then(() => res.json({ user: finalUser.toAuthJSON() }));
});

//POST login route (optional, everyone has access)
router.post("/login", auth.optional, (req, res, next) => {
  const {
    body: { user }
  } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: "is required"
      }
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: "is required"
      }
    });
  }

  return passport.authenticate(
    "local",
    { session: false },
    (err, passportUser, info) => {
      if (err) {
        return next(err);
      }

      if (passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();

        return res.json({ user: user.toAuthJSON() });
      }

      return status(400).info;
    }
  )(req, res, next);
});

//GET current route (required, only authenticated users have access)
router.get("/current", auth.required, (req, res, next) => {
  const {
    payload: { id }
  } = req;

  return Users.findById(id).then(user => {
    if (!user) {
      return res.sendStatus(400);
    }

    return res.json({ user: user.toAuthJSON() });
  });
});

module.exports = router;

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;

let mongoose = require("mongoose");
let Recipe = require("../model/Recipe.js");

function getRecipes(req, res, next) {
  Recipe.find({}, null, { sort: { name: 1 } }, function(err, products) {
    if (err) return next(err);
    res.json(products);
  });
}

function getRecipe(req, res, next) {
  Recipe.findById(req.params.id, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

function addRecipe(req, res, next) {
  Recipe.create(req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

function searchRecipes(req, res, next) {
  let category = req.params.category;
  let content = req.params.content;
  let query = {};
  query[category] = new RegExp(content, "i");
  console.log(query);
  Recipe.find(query, null, { sort: { name: 1 } }, function(err, products) {
    if (err) return next(err);
    res.json(products);
  });
}

function modifyRecipe(req, res, next) {
  Recipe.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

function deleteRecipe(req, res, next) {
  Recipe.findByIdAndRemove(req.params.id, req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
}

module.exports = {
  getRecipes,
  getRecipe,
  addRecipe,
  searchRecipes,
  modifyRecipe,
  deleteRecipe
};

var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Recipe = require("../model/Recipe.js");

let func = require("./recette");

/* GET ALL PRODUCTS */
router.get("/getrecipes", func.getRecipes);

/* GET SINGLE PRODUCT BY ID */
router.get("/getrecipe/:id", func.getRecipe);

/* GET PRODUCTS THROUGH A SEARCH OF A PARTICULAR FIELD */
router.get("/getrecipes/:category/:content", func.searchRecipes);

/* SAVE PRODUCT */
router.post("/addrecipe", func.addRecipe);

/* UPDATE PRODUCT */
router.put("/modify/:id", func.modifyRecipe);

/* DELETE PRODUCT */
router.delete("/delete/:id", func.deleteRecipe);

module.exports = router;

var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;

let mongoose = require("mongoose");

let RecipeSchema = new mongoose.Schema({
  name: String,
  difficulty: String,
  country: String,
  duration: String,
  type: String,
  ingredients: Array,
  explication: String,
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("recette", RecipeSchema);

const mongoose = require("mongoose");

const crypto = require("crypto");
const jwt = require("jsonwebtoken");

const UsersSchema = new mongoose.Schema({
  email: String,
  hash: String,
  salt: String
});

UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString("hex");
  this.hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
};

UsersSchema.methods.validatePassword = function(password) {
  const hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
  return this.hash === hash;
};

UsersSchema.methods.generateJWT = function() {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign(
    {
      email: this.email,
      id: this._id,
      exp: parseInt(expirationDate.getTime() / 1000, 10)
    },
    "secret"
  );
};

UsersSchema.methods.toAuthJSON = function() {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT()
  };
};

module.exports = mongoose.model("Users", UsersSchema);
