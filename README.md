Movie Database
===========

A basic CRUD application using MEAN stack allowing the user to manage a personal movie database.

Lancement de l'application avec Docker (recommandé)
===========

root : $ docker-compose up --build


Lancement de l'application sans Docker
===========

ouvrir trois consoles :
- la première pour exécuter la commande $ mongod
- la deuxième pour exécuter la commande $ cd livre-recette && npm install && ng serve --open
- la troisième pour exécuter la commande $ cd back/node-rest-api && npm install && npm start







COPYRIGHT
========

©Ajax